/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { SafeAreaView, Switch, View, Text } from 'react-native';
import TextComponent from './components/TextComponent'

class MenuComponent extends Component {
  constructor(props) {
    super(props);
    this.state = { component: "list" }
  }
  render() {

    return (
      <View style={{ flex: 1 }}>
        <View onPress={() => {

        }}></View>

      </View>
    );
  }
}
renderComponent()
{

  switch (this.state.component) {
    case 'TextComponent':
      return <TextComponent />
    default:
      return <View></View>
  }
}

const App = () => {

  return (
    <SafeAreaView style={{ flex: 1, }}>
      <MenuComponent />
      {this.renderComponent()}
    </SafeAreaView>
  );
};

export default App;
