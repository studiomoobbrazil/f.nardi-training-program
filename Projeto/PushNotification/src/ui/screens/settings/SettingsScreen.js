import AsyncStorage from '@react-native-community/async-storage';
import React, { Component } from 'react';
import { Alert, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Colors from '../../../common/Colors';
import Fonts from '../../../common/Fonts';
import Button from '../../components/Button';
import Header from '../../components/Header';

export default class SettingsScreen extends Component {
	state = { name: null, email: null, identifier: null };

	async componentDidMount() {
		const name = await AsyncStorage.getItem('user_name');
		const email = await AsyncStorage.getItem('user_email');
		const identifier = await AsyncStorage.getItem('user_identifier');

		this.setState({ name, email, identifier });
	}

	render() {
		return (
			<SafeAreaView style={{ flex: 1, backgroundColor: Colors.persianRed }}>
				<Header title={'Configurações'}></Header>
				<ScrollView style={{ flex: 1, backgroundColor: Colors.extraLightGray, padding: 15 }}>
					{/* NOME */}
					<View style={{ marginTop: 10 }}>
						<Text style={styles.formLabel}>Nome</Text>
						<View style={styles.formField}>
							<TextInput
								ref={input => {
									this.inputCode = input;
								}}
								style={styles.formTextInput}
								placeholder={'Digite o nome...'}
								autoCapitalize={'words'}
								autoCorrect={false}
								multiline={false}
								maxLength={100}
								onChangeText={text => this.setState({ name: text })}
								value={this.state.name}></TextInput>
						</View>
					</View>

					{/* E-MAIL */}
					<View style={{ marginTop: 10 }}>
						<Text style={styles.formLabel}>E-mail</Text>
						<View style={styles.formField}>
							<TextInput
								style={styles.formTextInput}
								placeholder={'Digite o e-mail...'}
								autoCapitalize={'none'}
								keyboardType={'email-address'}
								autoCorrect={false}
								multiline={false}
								maxLength={70}
								onChangeText={text => this.setState({ email: text })}
								value={this.state.email}></TextInput>
						</View>
					</View>

					{/* MATRÍCULA */}
					<View style={{ marginTop: 10 }}>
						<Text style={styles.formLabel}>Matrícula</Text>
						<View style={styles.formField}>
							<TextInput
								style={styles.formTextInput}
								placeholder={'Digite a matrícula...'}
								autoCapitalize={'none'}
								keyboardType={'number-pad'}
								autoCorrect={false}
								multiline={false}
								maxLength={10}
								onChangeText={text => this.setState({ identifier: text })}
								value={this.state.identifier}></TextInput>
						</View>
					</View>

					<Button
						title={'Salvar'}
						style={{ marginTop: 30, marginBottom: 40 }}
						onPress={() => {
							this.save();
						}}></Button>
				</ScrollView>
			</SafeAreaView>
		);
	}

	async save() {
		try {
			if (this.validate()) {
				await AsyncStorage.setItem('user_name', this.state.name);
				await AsyncStorage.setItem('user_email', this.state.email);

				if (this.state.identifier) {
					await AsyncStorage.setItem('user_identifier', this.state.identifier);
				}

				Alert.alert('Sucesso', 'Dados salvos com sucesso');
			}
		} catch (exception) {
			Alert.alert('Ops', 'Não foi possível salvar os dados. ' + exception.error);
		}
	}

	validate() {
		if (!this.state.name) {
			Alert.alert('Atenção', 'O campo nome deve ser preenchido.');
			return false;
		} else if (!this.state.email) {
			this.inputCode.focus();
			Alert.alert('Atenção', 'O e-mail deve ser preenchido.');
			return false;
		}

		return true;
	}
}

const styles = StyleSheet.create({
	formLabel: {
		fontFamily: Fonts.nunitoExtraBold,
		color: Colors.black,
		fontSize: 14,
	},

	formTextLabel: {
		flex: 1,
		fontFamily: Fonts.nunitoRegular,
		color: Colors.lightGray,
		fontSize: 14,
	},

	formField: {
		marginTop: 10,
		height: 48,
		backgroundColor: Colors.white,
		borderRadius: 4,
		alignItems: 'center',
		flexDirection: 'row',
	},

	formTextInput: {
		fontFamily: Fonts.nunitoRegular,
		color: Colors.black,
		fontSize: 14,
		flex: 1,
		paddingHorizontal: 15,
	},
});
