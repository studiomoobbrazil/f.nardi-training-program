import React, { Component } from 'react';
import { SafeAreaView, Text, View } from 'react-native';
import Colors from '../../../common/Colors';
import Header from '../../components/Header';

export default class SettingsScreen extends Component {
	render() {
		return (
			<SafeAreaView style={{ flex: 1, backgroundColor: Colors.persianRed }}>
				<Header title={'Configurações'}></Header>
				<View style={{ flex: 1, backgroundColor: Colors.white }}>
					<Text>Configurações</Text>
				</View>
			</SafeAreaView>
		);
	}
}
