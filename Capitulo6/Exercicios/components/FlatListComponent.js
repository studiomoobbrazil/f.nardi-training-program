import React, { Component } from 'react';
import { View, Text, FlatList, TouchableHighlight } from 'react-native';

class ListItem extends Component {
    constructor(props) {
        super();
    }
    render() {
        return (<View>
            <Text> {this.props.item.title}</Text>

        </View>)
    }
}

export default class FlatListComponent extends Component {

    constructor() {
        super();
        this.data = [{ id: "0", title: "Minas Gerais" }, { id: "1", title: "São Paulo" }, { id: "2", title: "Rio de Janeiro" }]
        this.state = { selected: "-" }
    }
    render() {
        return (<View style={{ flex: 1, padding: 10, }}>
            <Text style={{ textAlign: "center" }}>{this.constructor.name}</Text>
            <View style={{ flexDirection: "row" }}>
                <Text>Selected value: </Text>
                <Text>{this.state.selected}</Text>
            </View>
            <FlatList style={{ flex: 1 }} data={this.data} renderItem={({ item }) =>
                <TouchableHighlight onPress={() => {

                    this.setState({ selected: item.title })

                }}>
                    <ListItem item={item} />
                </TouchableHighlight>
            } keyExtractor={item => item.id} />

        </View>)
    }

}