import React, { Component } from 'react';
import { Text, TouchableWithoutFeedback, View } from 'react-native';
import Colors from '../../../common/Colors';
import Fonts from '../../../common/Fonts';
import RadioButton from '../../components/RadioButton';

export default class TypeListItem extends Component {
	state = { selected: false };

	render() {
		let data = this.props.data;
		return (
			<TouchableWithoutFeedback
				onPress={() => {
					this.onSelectItem();
				}}>
				<View style={{ marginTop: 10, height: 48, backgroundColor: Colors.white, borderRadius: 4, alignItems: 'center', flexDirection: 'row', padding: 15 }}>
					<Text style={{ flex: 1, fontFamily: Fonts.nunitoSemiBold, color: Colors.black, fontSize: 16, textTransform: 'uppercase' }}>{data.name}</Text>
					<RadioButton selected={this.state.selected}></RadioButton>
				</View>
			</TouchableWithoutFeedback>
		);
	}

	onSelectItem() {
		this.setState(previousState => ({ selected: !previousState.selected }));

		let data = this.props.data;
		this.props.onSelectItem(data);
	}
}
