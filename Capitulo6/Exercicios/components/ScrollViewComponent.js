import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions, Image } from 'react-native';

export default class ScrollViewComponent extends Component {

    constructor() {
        super();
    }
    render() {
        return (<View style={{ flex: 1, }}>
            <Text style={{ textAlign: "center" }}>{this.constructor.name}</Text>
            <ScrollView style={{ flex: 1 }} horizontal={true} pagingEnabled={true}>
                <Image style={{ width: Dimensions.get('window').width, height: '100%' }} source={{ uri: "https://img1.10bestmedia.com/Images/Photos/352450/GettyImages-913753556_55_660x440.jpg" }} />
                <Image style={{ width: Dimensions.get('window').width, height: '100%' }} source={{ uri: "https://img2.10bestmedia.com/Images/Photos/252183/p-8262728096-ff33d4d331-b_54_990x660_201404251432.jpg" }} />
                <Image style={{ width: Dimensions.get('window').width, height: '100%' }} source={{ uri: "https://img1.10bestmedia.com/Images/Photos/252255/p-5572159304-4e9dd17821-b_54_990x660_201406012216.jpg" }} />

            </ScrollView>
        </View>)
    }

}