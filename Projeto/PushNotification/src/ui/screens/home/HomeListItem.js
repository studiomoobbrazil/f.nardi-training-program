import { format, fromUnixTime } from 'date-fns';
import React, { Component } from 'react';
import { Image, Text, TouchableWithoutFeedback, View } from 'react-native';
import Colors from '../../../common/Colors';
import Fonts from '../../../common/Fonts';
import Images from '../../../common/Images';

export default class HomeListItem extends Component {
	constructor(props) {
		super(props);

		this.data = props.data;
		this.formattedDate = format(fromUnixTime(this.data.date), 'dd/MM/yyyy HH:mm');
	}

	render() {
		return (
			<TouchableWithoutFeedback
				onPress={() => {
					this.onSelectItem();
				}}>
				<View style={{ height: 220, backgroundColor: Colors.white, borderRadius: 6 }}>
					{!this.data.photo && (
						<View style={{ height: 150, borderTopLeftRadius: 6, borderTopRightRadius: 6, backgroundColor: Colors.lightGray, alignItems: 'center', justifyContent: 'center' }}>
							<Image source={Images.icPlaceholderRow}></Image>
						</View>
					)}
					{this.data.photo && <Image style={{ height: 150, borderTopLeftRadius: 6, borderTopRightRadius: 6 }} resizeMode={'cover'} source={{ uri: this.data.photo }}></Image>}
					<View style={{ flex: 1, paddingHorizontal: 15, paddingVertical: 10 }}>
						<Text style={{ flex: 1, fontFamily: Fonts.nunitoSemiBold, color: Colors.black, fontSize: 18, textTransform: 'uppercase' }}>{`${this.data.code} | ${this.data.type}`}</Text>
						<Text style={{ flex: 1, fontFamily: Fonts.nunitoRegular, color: Colors.mediumGray, fontSize: 14, textTransform: 'uppercase' }}>{this.formattedDate}</Text>
					</View>
				</View>
			</TouchableWithoutFeedback>
		);
	}

	onSelectItem() {
		this.props.onSelectItem(this.data);
	}
}
