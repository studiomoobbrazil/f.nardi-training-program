/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  View,
  
} from 'react-native';

const App: () => React$Node = () => {
  return (
    <>
      <View style={{ flex: 1,backgroundColor: "yellow" }}>
        <View style={{ width:50,height:50,position:"absolute",backgroundColor: "red",left:10,top:0 }}></View>
        <View style={{ width:50,height:50,position:"absolute",backgroundColor: "green",right:10,top:30 }}></View>
        <View style={{ width:50,height:50,position:"absolute",backgroundColor: "blue",bottom:-25,right:-25 }}></View>
        
      </View>
    </>
  );
};

export default App;
