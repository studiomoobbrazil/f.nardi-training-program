/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  View,
  
} from 'react-native';

const App: () => React$Node = () => {
  return (
    <>
      <View style={{ flex: 1 ,flexDirection: 'column',justifyContent: 'flex-start',}}>
        <View style={{ width:50,height:50, backgroundColor: "red" }}></View>
        <View style={{ width:50,height:50, backgroundColor: "yellow" }}></View>
        <View style={{ width:50,height:50, backgroundColor: "green" }}></View>

      </View>
    </>
  );
};

export default App;
