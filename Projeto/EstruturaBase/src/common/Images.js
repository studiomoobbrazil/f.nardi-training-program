const basePath = '../assets/images/';

const Images = {
    icArrowRight: require(`${basePath}ic_arrow_right.png`),
	icBack: require(`${basePath}ic_back.png`),
    icBottomBarHomeOn: require(`${basePath}ic_bottom_bar_home_on.png`),
    icBottomBarHomeOff: require(`${basePath}ic_bottom_bar_home_off.png`),
    icBottomBarNewOn: require(`${basePath}ic_bottom_bar_new_on.png`),
	icBottomBarNewOff: require(`${basePath}ic_bottom_bar_new_off.png`),    
    icBottomBarSettingsOn: require(`${basePath}ic_bottom_bar_settings_on.png`),
    icBottomBarSettingsOff: require(`${basePath}ic_bottom_bar_settings_off.png`),
	icCamera: require(`${basePath}ic_camera.png`),
	icLocation: require(`${basePath}ic_location.png`),
	ic_PlaceholderEmptyData: require(`${basePath}ic_placeholder_empty_data.png`),
	logoSplash: require(`${basePath}logo_splash.png`),        
};

export default Images;