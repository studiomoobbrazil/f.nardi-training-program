const basePath = '../assets/images/';

const Images = {
    icArrowRight: require(`${basePath}ic_arrow_right.png`),
	icBack: require(`${basePath}ic_back.png`),
    icBottomBarHomeOn: require(`${basePath}ic_bottom_bar_home_on.png`),
    icBottomBarHomeOff: require(`${basePath}ic_bottom_bar_home_off.png`),
    icBottomBarNewOn: require(`${basePath}ic_bottom_bar_new_on.png`),
	icBottomBarNewOff: require(`${basePath}ic_bottom_bar_new_off.png`),    
    icBottomBarSettingsOn: require(`${basePath}ic_bottom_bar_settings_on.png`),
    icBottomBarSettingsOff: require(`${basePath}ic_bottom_bar_settings_off.png`),
	icCamera: require(`${basePath}ic_camera.png`),
	icLocation: require(`${basePath}ic_location.png`),
    icPlaceholderEmptyData: require(`${basePath}ic_placeholder_empty_data.png`),
    icPlaceholderRow: require(`${basePath}ic_placeholder_row.png`),
    logoSplash: require(`${basePath}logo_splash.png`),        
    icOptions: require(`${basePath}ic_options.png`),
};

export default Images;