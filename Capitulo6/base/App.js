/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { SafeAreaView, View, ScrollView, Text } from 'react-native';
import TextComponent from './components/TextComponent'
import ButtonComponent from './components/ButtonComponent'
import ImageComponent from './components/ImageComponent'
import PickerComponent from './components/PickerComponent'
import ScrollViewComponent from './components/ScrollViewComponent'
import SwitchComponent from './components/SwitchComponent'
import TextInputComponent from './components/TextInputComponent'
import TouchsComponent from './components/TouchsComponent'

class ContainerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = { component: "" }
  }
  render() {

    return (
      <View style={{ flex: 1 }}>
        <View>
          <ScrollView style={{ padding: 5 }} horizontal={true}>
            <Text style={{ borderColor: "#000000", borderWidth: 1, padding: 5, marginRight: 5, }} onPress={() => {
              this.setState({ component: "TextComponent" })
            }}> TextComponent</Text>

            <Text style={{ borderColor: "#000000", borderWidth: 1, padding: 5, marginRight: 10, }} onPress={() => {
              this.setState({ component: "ImageComponent" })
            }}> ImageComponent</Text>

            <Text style={{ borderColor: "#000000", borderWidth: 1, padding: 5, marginRight: 10, }} onPress={() => {
              this.setState({ component: "ScrollViewComponent" })
            }}> ScrollViewComponent</Text>

            <Text style={{ borderColor: "#000000", borderWidth: 1, padding: 5, marginRight: 10, }} onPress={() => {
              this.setState({ component: "TextInputComponent" })
            }}> TextInputComponent</Text>

            <Text style={{ borderColor: "#000000", borderWidth: 1, padding: 5, marginRight: 10, }} onPress={() => {
              this.setState({ component: "ButtonComponent" })
            }}> ButtonComponent</Text>

            <Text style={{ borderColor: "#000000", borderWidth: 1, padding: 5, marginRight: 10, }} onPress={() => {
              this.setState({ component: "PickerComponent" })
            }}> PickerComponent</Text>

            <Text style={{ borderColor: "#000000", borderWidth: 1, padding: 5, marginRight: 10, }} onPress={() => {
              this.setState({ component: "SwitchComponent" })
            }}> SwitchComponent</Text>

            <Text style={{ borderColor: "#000000", borderWidth: 1, padding: 5, marginRight: 10, }} onPress={() => {
              this.setState({ component: "TouchsComponent" })
            }}> TouchsComponent</Text>

          </ScrollView>
        </View>

        <View style={{ flex: 1 }}>
          {this.renderComponent()}
        </View>

      </View>
    );
  }

  renderComponent() {

    switch (this.state.component) {
      case 'TextComponent':
        return <TextComponent />
      case 'ButtonComponent':
        return <ButtonComponent />
      case 'ImageComponent':
        return <ImageComponent />
      case 'PickerComponent':
        return <PickerComponent />
      case 'ScrollViewComponent':
        return <ScrollViewComponent />
      case 'SwitchComponent':
        return <SwitchComponent />
      case 'TextComponent':
        return <TextComponent />
      case 'TextInputComponent':
        return <TextInputComponent />
      case 'TouchsComponent':
        return <TouchsComponent />
      default:
        return <View></View>
    }
  }

}

const App = () => {

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ContainerComponent />

    </SafeAreaView>
  );
};

export default App;