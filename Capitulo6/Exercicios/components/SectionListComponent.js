import React, { Component } from 'react';
import { View, Text, SectionList, TouchableHighlight } from 'react-native';

class ListItem extends Component {
    constructor(props) {
        super();
    }
    render() {
        return (<View>
            <Text> {this.props.item.title}</Text>

        </View>)
    }
}

class ListHeader extends Component {
    constructor(props) {
        super();
    }
    render() {
        return (<View>
            <Text style={{ fontWeight: 'bold' }}> {this.props.title}</Text>

        </View>)
    }
}

export default class SectionListComponent extends Component {

    constructor() {
        super();
        this.data = [
            {
                title: "Sudeste",
                data: [{ id: "0", title: "Minas Gerais" }, { id: "1", title: "São Paulo" }, { id: "2", title: "Rio de Janeiro" }]
            },
            {
                title: "Norte",
                data: [{ id: "3", title: "Acre" }, { id: "4", title: "Amazonas" }, { id: "5", title: "Pará" }]
            }
        ]
        this.state = { selected: "-" }
    }
    render() {
        return (<View style={{ flex: 1, padding: 10, }}>
            <Text style={{ textAlign: "center" }}>{this.constructor.name}</Text>
            <View style={{ flexDirection: "row" }}>
                <Text>Selected value: </Text>
                <Text>{this.state.selected}</Text>
            </View>
            <SectionList style={{ flex: 1 }} sections={this.data} renderItem={({ item }) =>
                <TouchableHighlight onPress={() => {

                    this.setState({ selected: item.title })

                }}>
                    <ListItem item={item} />
                </TouchableHighlight>
            }
                renderSectionHeader={({ section: { title } }) => (
                    <ListHeader title={title} />
                )}

                keyExtractor={(item, index) => item.id + index} />

        </View>)
    }

}