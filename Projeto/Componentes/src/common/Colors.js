const Colors = {
	white: '#fff',
	black: '#000',
	persianRed: '#FF4630',
	mediumGray: '#606060',
    lightGray: '#F4F4F4',
    
	debug1: '#0220ff',
	debug2: '#ff8001',
	debug3: '#ff0000',
	debug4: '#0ef702',
	debug5: '#9d06e8',
	debug6: '#e806af',
};

export default Colors;