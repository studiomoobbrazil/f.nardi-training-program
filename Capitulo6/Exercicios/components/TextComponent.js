import React, { Component } from 'react';
import { View, Text } from 'react-native';

export default class TextComponet extends Component {

    constructor() {
        super();
    }
    render() {
        return (<View style={{ flex: 1, padding: 10, }}>
            <Text style={{ textAlign: "center" }}>{this.constructor.name}</Text>

            <View style={{ flexDirection: "row" }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Nome: </Text>
                <Text style={{ fontSize: 20 }}>João Liberato </Text>
            </View>
            <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'red' }}>Descrição: </Text>
            <Text style={{ fontSize: 15 }}>Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</Text>
        </View>)
    }

}