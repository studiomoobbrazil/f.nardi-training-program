import React from 'react';
import { Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Images from '../common/Images';
import HomeScreen from '../ui/screens/home/HomeScreen';
import NewScreen from '../ui/screens/new/NewScreen';
import TypeSelectionScreen from '../ui/screens/new/TypeSelectionScreen';
import SettingsScreen from '../ui/screens/settings/SettingsScreen';
import DetailScreen from '../ui/screens/home/DetailScreen';
import EditScreen from '../ui/screens/new/EditScreen';

/******************************************************
 * HOME
 ******************************************************/

const HomeStack = createStackNavigator(
	{
		Home: HomeScreen,
		Detail: DetailScreen,
		Edit: EditScreen,
		TypeSelection:TypeSelectionScreen,
	},
	{
		headerMode: 'none',
	}
);
HomeStack.navigationOptions = {    
    tabBarIcon: ({ focused }) => (
        focused ?
        <Image source={Images.icBottomBarHomeOn} />:
        <Image source={Images.icBottomBarHomeOff} />
      ),    
};

/******************************************************
 * NEW
 ******************************************************/

const NewStack = createStackNavigator(
	{
		New: NewScreen,
		TypeSelection:TypeSelectionScreen,
	},
	{
		headerMode: 'none',
	}
);
NewStack.navigationOptions = {
    tabBarIcon: ({ focused }) => (
        focused ?
        <Image source={Images.icBottomBarNewOn} />:
        <Image source={Images.icBottomBarNewOff} />
      ),    
};

/******************************************************
 * SETTINGS
 ******************************************************/

const SettingsStack = createStackNavigator(
	{
		Settings: SettingsScreen,
	},
	{
		headerMode: 'none',
	}
);
SettingsStack.navigationOptions = {
    tabBarIcon: ({ focused }) => (
        focused ?
        <Image source={Images.icBottomBarSettingsOn} />:
        <Image source={Images.icBottomBarSettingsOff} />
      ),    
};

/******************************************************
 * MAIN
 ******************************************************/
const MainRoute = createBottomTabNavigator(
	{
		Home: HomeStack,
		New: NewStack,
		Settings: SettingsStack,
	},
	{
		tabBarOptions: {
            showLabel:false,
        },
        headerMode: 'none',        
	}
);

export default createAppContainer(MainRoute);
