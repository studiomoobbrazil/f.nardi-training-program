import React, { Component } from 'react';
import { SafeAreaView, Text, View } from 'react-native';
import Colors from '../../../common/Colors';
import Header from '../../components/Header';
import Button from '../../components/Button';

export default class HomeScreen extends Component {
	render() {
		return (
			<SafeAreaView style={{ flex: 1, backgroundColor: Colors.persianRed }}>
				<Header title={'Minhas inspeções'}></Header>
				<View style={{ flex: 1, backgroundColor: Colors.white }}>
					<Text>Home</Text>
					<Button title={'Enviar'} style={{margin:20}}></Button>
				</View>
			</SafeAreaView>
		);
	}
}