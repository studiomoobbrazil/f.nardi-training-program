import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import Colors from '../../common/Colors';
import Fonts from '../../common/Fonts';

export default class Button extends Component {
	render() {
		const { onPress, style, title } = this.props;

		return (
			<TouchableOpacity activeOpacity={0.8} onPress={onPress} style={[style, styles.buttonContainer]}>
				<Text style={styles.buttonText}>{title}</Text>
			</TouchableOpacity>
		);
	}
}

const styles = StyleSheet.create({
	buttonContainer: {
		height: 52,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: Colors.persianRed,
		borderRadius: 6,
	},

	buttonText: {
		fontFamily: Fonts.nunitoExtraBold,
		color: Colors.white,
		fontSize: 22,
	},
});
