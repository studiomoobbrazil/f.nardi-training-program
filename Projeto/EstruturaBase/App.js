/**
 * @format
 * @flow
 */

import React from 'react';
import Routes from "./src/route/Routes";

const App: () => React$Node = () => {
  return (<Routes />);
};

export default App;
