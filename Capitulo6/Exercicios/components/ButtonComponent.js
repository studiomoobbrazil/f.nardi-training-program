import React, { Component } from 'react';
import { View, Text, TextInput, Button } from 'react-native';

export default class ButtonComponent extends Component {

    constructor() {
        super();
        this.state = { username: '', password: '' }
    }
    render() {
        return (<View style={{ flex: 1, padding: 10, }}>
            <Text style={{ textAlign: "center" }}>{this.constructor.name}</Text>
            <TextInput style={{ height: 40, marginHorizontal: 10, borderColor: 'gray', borderWidth: 1, padding: 5 }} placeholder="Entre com seu email..." keyboardType="email-address" onChangeText={(text) => {

                this.setState({ username: text })

            }}></TextInput>
            <TextInput secureTextEntry={true} style={{ height: 40, marginHorizontal: 10, borderColor: 'gray', borderWidth: 1, padding: 5, marginTop: 10,marginBottom: 20, }} placeholder="Entre com sua senha..." keyboardType="default" onChangeText={(text) => {

                this.setState({ password: text })

            }}></TextInput>

            <Button title="Login" style={{ height: 40, margin: 20}} onPress={()=>{

                if(this.state.username == "dev@mrn.com.br" && this.state.password == "123456")
                {
                    alert("Login realizado com sucesso!")
                }
                else
                {
                    alert("Dados incorretos!")
                }

            }}/>

        </View>)
    }

}