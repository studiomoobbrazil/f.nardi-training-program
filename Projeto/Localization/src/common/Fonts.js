const Fonts = {
	nunitoRegular: 'Nunito-Regular',
	nunitoSemiBold: 'Nunito-SemiBold',
	nunitoExtraBold: 'Nunito-ExtraBold',
};

export default Fonts;