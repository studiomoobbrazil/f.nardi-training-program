import axios from 'axios';
import React, { Component } from 'react';
import { ActivityIndicator, Alert, FlatList, SafeAreaView, View } from 'react-native';
import Colors from '../../../common/Colors';
import Images from '../../../common/Images';
import Header from '../../components/Header';
import TypeListItem from './TypeListItem';

export default class TypeSelectionScreen extends Component {
	state = { data: [], loading: false };

	componentDidMount() {
		this.getData();
	}

	render() {
		return (
			<SafeAreaView style={{ flex: 1, backgroundColor: Colors.persianRed }}>
				<Header
					title={'Tipo'}
					leftButtonIcon={Images.icBack}
					leftButtonAction={() => {
						this.backAction();
					}}></Header>
				<View style={{ flex: 1, backgroundColor: Colors.extraLightGray, padding: 15 }}>
					{this.state.loading && (
						<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
							<ActivityIndicator style={{ position: 'absolute', alignSelf: 'center' }} animating={this.state.loading} size="large" color={Colors.persianRed} />
						</View>
					)}
					{!this.state.loading && (
						<FlatList
							style={{ flex: 1 }}
							data={this.state.data}
							renderItem={({ item }) => (
								<TypeListItem
									onSelectItem={data => {
										this.onSelectItem(data);
									}}
									data={item}
								/>
							)}
							keyExtractor={item => item.id.toString()}></FlatList>
					)}
				</View>
			</SafeAreaView>
		);
	}

	async getData() {
		try {
			this.setState({ loading: true });

			const url = 'http://www.studiomoob.com/types.json';

			const response = await axios.get(url);

			this.setState({ data: response.data });
		} catch (exception) {
			Alert.alert('Ops', 'Não foi possível buscar os tipos. ' + exception);
		} finally {
			this.setState({ loading: false });
		}
	}

	onSelectItem(data) {
		const onSelect = this.props.navigation.getParam('onSelect');

		if (onSelect) {
			setTimeout(() => {
				onSelect(data);
				this.backAction();
			}, 100);
		}
	}

	backAction() {
		this.props.navigation.goBack(null);
	}
}
