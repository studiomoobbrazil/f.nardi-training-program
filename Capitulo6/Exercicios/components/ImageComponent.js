import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';

export default class ImageComponent extends Component {

    constructor() {
        super();
        this.state = { status: "Not Loaded..." }
    }
    render() {
        return (<View style={{ flex: 1, padding: 10, }}>
            <Text style={{ textAlign: "center" }}>{this.constructor.name}</Text>            
            <View style={{ flexDirection: "column" }}>
            <View style={{ flexDirection: "row" }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Status: </Text>
                <Text style={{ fontSize: 20 }}>{this.state.status} </Text>
            </View>
                <Image style={{ width: 320, height: 320 }} source={{ uri: "https://upload.wikimedia.org/wikipedia/commons/a/a9/Big_Banner_Example.png" }}
                    onLoadStart={() => {

                        this.setState({ "status": "Start..." })

                    }}
                    onLoadEnd={() => {

                        this.setState({ "status": "Finish!" })

                    }}
                />
            </View>

        </View>)
    }

}