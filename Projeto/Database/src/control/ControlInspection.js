import Database from '../common/Database';
import AsyncStorage from '@react-native-community/async-storage';

export default class ControlInspection {
	static async insert(data) {
		try {
			const SQL = 'INSERT INTO "inspection" ("type", "code", "description", "date", "photo", "user", "location") VALUES (?, ?, ?, ?, ?, ?, ?)';

			const date = Math.round(new Date().getTime() / 1000);

			const userName = await AsyncStorage.getItem('user_name');

			const params = [data.type, data.code, data.description, date, data.photo, userName, data.location];

			return await Database.insert(SQL, params);
		} catch (exception) {
			console.log(exception);
		}
	}

	static async saveAPI(data) {
		try {
			const url = 'http://www.fnardi.com/inspection';
			let params = { "type": data.type, "code": data.code, "description": data.description, "date": date, "photo": data.photo, "username": userName, "location": data.location }

			const response = await axios.post(url, params);
			if (response.status == 200) {
				return true;
			}
			else {
				return false;
			}

		} catch (exception) {
			console.log(exception);
		}
	}
	static async update(data) {
		try {
			const SQL = 'UPDATE "inspection" SET "type" = ?, "code" = ?, "description" = ?, "photo" = ?, "location" = ? WHERE "id" = ?';

			const params = [data.type, data.code, data.description, data.photo, data.location, data.id];

			return await Database.update(SQL, params);
		} catch (exception) {
			console.log(exception);
		}
	}
	static async search(term) {
		try {
			const SQL = 'SELECT * FROM "inspection" WHERE "code" = ? ORDER BY "date" DESC';

			const results = await Database.select(SQL, [term]);

			const data = results.rows.raw().map(row => {
				return {
					id: row.id,
					type: row.type,
					code: row.code,
					description: row.description,
					date: row.date,
					photo: row.photo,
					user: row.user,
					location: row.location,
				};
			});

			return data;
		} catch (exception) {
			console.log(exception);
		}
	}
	static async list() {
		try {
			const SQL = 'SELECT * FROM "inspection" ORDER BY "date" DESC';

			const results = await Database.select(SQL);

			const data = results.rows.raw().map(row => {
				return {
					id: row.id,
					type: row.type,
					code: row.code,
					description: row.description,
					date: row.date,
					photo: row.photo,
					user: row.user,
					location: row.location,
				};
			});

			return data;
		} catch (exception) {
			console.log(exception);
		}
	}

	static async delete(id) {
		try {
			const SQL = 'DELETE FROM "inspection" WHERE "id" = ?';

			const params = [id];

			return await Database.delete(SQL, params);
		} catch (exception) {
			console.log(exception);
		}
	}
}
