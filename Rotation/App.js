/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  View,

} from 'react-native';

const App: () => React$Node = () => {
  return (
    <>

      <SafeAreaView style={{flex:1}}>
        <View style={{flex:1,padding:50}}>
          <View style={{flex:1,borderWidth:1,borderColor:"#FFCC00"}}></View>
        </View>

      </SafeAreaView>
    </>
  );
};

export default App;
