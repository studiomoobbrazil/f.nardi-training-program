import React, { Component } from 'react';
import { View, Text, Picker } from 'react-native';

export default class PickerComponent extends Component {

    constructor() {
        super();
        this.state = { period: "-" }
    }
    render() {
        return (<View style={{ flex: 1, padding: 10, }}>
            <Text style={{ textAlign: "center" }}>{this.constructor.name}</Text>

            <Text style={{ textAlign: "center" }}>{this.state.period}</Text>
            <Picker
                mode="dropdown"
                style={{ height: 50, width: "100%", margin: 20 }}
                seletedValue={this.state.period}
                onValueChange={(value, index) =>
                    this.setState({ period: value })
                }>

                <Picker.Item label="Diurno" value="Diurno" />
                <Picker.Item label="Noturno" value="Noturno" />
                <Picker.Item label="Plantonista" value="Plantonista" />

            </Picker>

        </View>)
    }

}