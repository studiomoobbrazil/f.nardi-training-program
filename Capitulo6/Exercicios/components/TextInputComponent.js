import React, { Component } from 'react';
import { View,Text,TextInput } from 'react-native';

export default class TextInputComponent extends Component {

    constructor() {
        super();
        this.state = {color:'#000000'}
    }
    render() {
        return (<View style={{ flex: 1,padding: 10, }}>
            <Text style={{textAlign:"center"}}>{this.constructor.name}</Text>
             <TextInput style={{height:40,marginHorizontal:10,borderColor:'gray',borderWidth:1,padding:5}} placeholder="Entre com a cor do elemento..." onChangeText={(text)=>{

                if(text.length == 7)
                {
                    this.setState({color:text})
                }

             }}></TextInput>

             <View style={{height:40,marginHorizontal:10,marginTop: 10,padding:5,backgroundColor:this.state.color}}></View>

        </View>)
    }

}