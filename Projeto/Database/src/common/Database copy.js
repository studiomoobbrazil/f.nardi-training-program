


import SQLite from 'react-native-sqlite-storage';

export default class DatabaseCopy {

    static instance = null;

	static open() {
		return new Promise(function(resolve, reject) {
			if (instance == null) {
				instance = SQLite.openDatabase(
					{ name: 'db_inspector', createFromLocation: 1 },
					() => {
						resolve(true);
					},
					error => {
						reject(error);
					}
				);
			}
		});
	}

	static select(sql) {
		return new Promise(function(resolve, reject) {
			instance.transaction(sqliteTransaction => {
				try {
					sqliteTransaction.executeSql(
						sql,
						[],
						(sqliteTransaction, results) => {
							resolve(results);
						},
						err => {
							reject(err);
						}
					);
				} catch (exception) {
					reject(exception);
				}
			});
		});
	}

	static insert(sql) {
		return new Promise(function(resolve, reject) {
			instance.transaction(sqliteTransaction => {
				try {
					sqliteTransaction.executeSql(
						sql,
						[],
						(sqliteTransaction, results) => {
							resolve(results.insertId);
						},
						err => {
							reject(err);
						}
					);
				} catch (exception) {
					reject(exception);
				}
			});
		});
	}

	static update(sql) {
		return new Promise(function(resolve, reject) {
			instance.transaction(sqliteTransaction => {
				try {
					sqliteTransaction.executeSql(
						sql,
						[],
						(sqliteTransaction, results) => {
							resolve(true);
						},
						err => {
							reject(err);
						}
					);
				} catch (exception) {
					reject(exception);
				}
			});
		});
	}

	static delete(sql) {
		return new Promise(function(resolve, reject) {
			instance.transaction(sqliteTransaction => {
				try {
					sqliteTransaction.executeSql(
						sql,
						[],
						(sqliteTransaction, results) => {
							resolve(true);
						},
						err => {
							reject(err);
						}
					);
				} catch (exception) {
					reject(exception);
				}
			});
		});
	}

	static bulk(sql) {
		return new Promise(function(resolve, reject) {
			instance.transaction(sqliteTransaction => {
				try {
					sqliteTransaction.executeSql(
						sql,
						[],
						(sqliteTransaction, results) => {
							resolve(true);
						},
						err => {
							reject(err);
						}
					);
				} catch (exception) {
					reject(exception);
				}
			});
		});
    }
    
    static execute(sql) {
		return new Promise(function(resolve, reject) {
			instance.transaction(sqliteTransaction => {
				try {
					sqliteTransaction.executeSql(
						sql,
						[],
						(sqliteTransaction, results) => {
							resolve(true);
						},
						err => {
							reject(err);
						}
					);
				} catch (exception) {
					reject(exception);
				}
			});
		});
	}
}
