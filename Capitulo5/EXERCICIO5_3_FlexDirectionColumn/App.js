/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  View,

} from 'react-native';

const App: () => React$Node = () => {
  return (
    <>
      <View style={{ flex: 1, flexDirection: 'column', }}>
        <View style={{ flex: 1, backgroundColor: "red" }}></View>
        <View style={{ flex: 2, backgroundColor: "yellow" }}></View>
        <View style={{ flex: 3, backgroundColor: "green" }}></View>

      </View>
    </>
  );
};

export default App;
