import React, { Component } from 'react';
import { ActivityIndicator, FlatList, Image, SafeAreaView, Text, View } from 'react-native';
import Colors from '../../../common/Colors';
import Fonts from '../../../common/Fonts';
import Images from '../../../common/Images';
import ControlInspection from '../../../control/ControlInspection';
import Header from '../../components/Header';
import HomeListItem from './HomeListItem';

export default class HomeScreen extends Component {
	state = { data: [], loading: false };

	constructor(props) {
		super(props);

		this.didFocusListener = this.props.navigation.addListener('didFocus', () => this.getData());
	}

	componentWillUnmount() {
		this.didFocusListener.remove();
	}

	render() {
		return (
			<SafeAreaView style={{ flex: 1, backgroundColor: Colors.persianRed }}>
				<Header title={'Minhas inspeções'}></Header>
				<View style={{ flex: 1, backgroundColor: Colors.extraLightGray, padding: 15 }}>
					{this.state.loading && (
						<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
							<ActivityIndicator style={{ position: 'absolute', alignSelf: 'center' }} animating={this.state.loading} size="large" color={Colors.persianRed} />
						</View>
					)}
					{!this.state.loading && (
						<FlatList
							contentContainerStyle={{ flex: 1 }}
							style={{ flex: 1 }}
							data={this.state.data}
							ItemSeparatorComponent={this.renderSeparator}
							ListEmptyComponent={this.renderEmpty}
							renderItem={({ item }) => (
								<HomeListItem
									onSelectItem={data => {
										this.onSelectItem(data);
									}}
									data={item}
								/>
							)}
							keyExtractor={item => item.id.toString()}></FlatList>
					)}
				</View>
			</SafeAreaView>
		);
	}

	renderItem = () => {
		return <View style={{ height: 15 }}></View>;
	};

	renderSeparator = () => {
		return <View style={{ height: 15 }}></View>;
	};

	renderEmpty = () => {
		return (
			<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
				<View style={{ height: 110, alignItems: 'center' }}>
					<Image source={Images.icPlaceholderEmptyData}></Image>
					<Text style={{ marginTop: 5, textAlign: 'center', flex: 1, fontFamily: Fonts.nunitoExtraBold, color: Colors.black, fontSize: 18, lineHeight: 20 }}>Nenhuma inspeção{'\n'}encontrada</Text>
				</View>
			</View>
		);
	};

	async getData() {
		try {
			this.setState({ loading: true });

			const result = await ControlInspection.list();

			this.setState({ data: result });
		} catch (exception) {
			Alert.alert('Ops', 'Não foi possível buscar as inspeções. ' + exception);
		} finally {
			this.setState({ loading: false });
		}
	}

	onSelectItem(data) {

		this.props.navigation.navigate('Detail', {data:data});

	}
}
