import React, { Component } from 'react';
import { Image, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../../common/Colors';
import Fonts from '../../common/Fonts';

export default class Header extends Component {
	render() {
		const { leftButtonIcon, leftButtonAction, rightButtonIcon, rightButtonAction, title } = this.props;

		return (
			<View style={{ height: 50, backgroundColor: Colors.persianRed }}>
				<StatusBar backgroundColor={Colors.persianRed} barStyle="light-content" />
				<View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
					<View style={styles.iconButtonContainer}>
						{leftButtonIcon && leftButtonAction && (
							<TouchableOpacity style={styles.iconButton} onPress={leftButtonAction}>
								<Image source={leftButtonIcon} />
							</TouchableOpacity>
						)}
					</View>
					<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
						<Text style={{ fontSize: 22, color: Colors.white, fontFamily: Fonts.nunitoExtraBold }}>{title}</Text>
					</View>
					<View style={styles.iconButtonContainer}>
						{rightButtonIcon && rightButtonAction && (
							<TouchableOpacity style={styles.iconButton} onPress={rightButtonAction}>
								<Image source={rightButtonIcon} />
							</TouchableOpacity>
						)}
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	iconButtonContainer: {
		width: 44,
		height: 44,
	},

	iconButton: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
});
