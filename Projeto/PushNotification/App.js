/**
 * @format
 * @flow
 */

import React from 'react';
import Routes from "./src/route/Routes";
import Database from "./src/common/Database";
import { firebase } from '@react-native-firebase/messaging';
		
const App: () => React$Node = () => {

  firebase.messaging().subscribeToTopic('news');
  Database.open();
  return (<Routes />);
};

export default App;
