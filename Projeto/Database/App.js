/**
 * @format
 * @flow
 */

import React from 'react';
import Routes from "./src/route/Routes";
import Database from "./src/common/Database";
		

const App: () => React$Node = () => {
  Database.open();
  return (<Routes />);
};

export default App;
