import React, { Component } from 'react';
import { View } from 'react-native';
import Colors from '../../common/Colors';

export default class RadioButton extends Component {
	state = { selected: false };

	render() {
		const { selected } = this.props;

		return (
			<View style={{ width: 22, height: 22, borderRadius: 11, borderWidth: 2, borderColor: Colors.persianRed, backgroundColor: Colors.white, alignItems: 'center', justifyContent: 'center' }}>
				{selected && <View style={{ width: 10, height: 10, borderRadius: 5, backgroundColor: Colors.persianRed }}></View>}
			</View>
		);
	}
}
