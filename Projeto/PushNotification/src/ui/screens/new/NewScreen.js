import AsyncStorage from '@react-native-community/async-storage';
import React, { Component } from 'react';
import { Alert, Image, SafeAreaView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';
import RNFS from 'react-native-fs';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import Colors from '../../../common/Colors';
import Fonts from '../../../common/Fonts';
import Images from '../../../common/Images';
import ControlInspection from '../../../control/ControlInspection';
import Button from '../../components/Button';
import Header from '../../components/Header';

export default class NewScreen extends Component {
	state = { type: null, code: null, description: null, date: null, photoUri: null, photoPath: null, user: null, location: null };

	async componentDidMount() {
		const userName = await AsyncStorage.getItem('user_name');

		if (!userName) {
			Alert.alert(
				'Inspector',
				'Para preencher uma inspeção você primeiro configurar os dados do usuário. Deseja fazer isso agora?',
				[
					{
						text: 'Sim',
						onPress: () => {
							this.props.navigation.navigate('Settings');
						},
					},
					{
						text: 'Não',
						style: 'cancel',
					},
				],
				{ cancelable: true }
			);
		}
	}

	render() {
		return (
			<SafeAreaView style={{ flex: 1, backgroundColor: Colors.persianRed }}>
				<Header title={'Nova inspeção'}></Header>
				<ScrollView style={{ flex: 1, backgroundColor: Colors.extraLightGray, padding: 15 }}>
					{/* TIPO */}
					<TouchableWithoutFeedback
						onPress={() => {
							this.props.navigation.navigate('TypeSelection', {
								onSelect: item => {
									this.setState({ type: item.name });
								},
							});
						}}>
						<View style={{}}>
							<Text style={styles.formLabel}>Tipo</Text>
							<View style={[styles.formField]}>
								<TextInput style={[styles.formTextInput, { textTransform: 'uppercase' }]} placeholder={'Selecione o tipo de equipamento...'} editable={false} value={this.state.type}></TextInput>
								<Image style={{ marginRight: 10 }} source={Images.icArrowRight}></Image>
							</View>
						</View>
					</TouchableWithoutFeedback>

					{/* CÓDIGO */}
					<View style={{ marginTop: 10 }}>
						<Text style={styles.formLabel}>Código</Text>
						<View style={styles.formField}>
							<TextInput
								ref={input => {
									this.inputCode = input;
								}}
								style={styles.formTextInput}
								placeholder={'Digite o código do equipamento...'}
								autoCapitalize={'characters'}
								autoCorrect={false}
								multiline={false}
								maxLength={10}
								onChangeText={text => this.setState({ code: text })}
								value={this.state.code}></TextInput>
						</View>
					</View>

					{/* ANOMALIAS */}
					<View style={{ marginTop: 10 }}>
						<Text style={styles.formLabel}>Anomalias</Text>
						<View style={[styles.formField, { height: 200 }]}>
							<TextInput
								ref={input => {
									this.inputDescription = input;
								}}
								style={[styles.formTextInput, { height: 200 }]}
								textAlignVertical={'top'}
								placeholder={'Descreva as anomalias encontradas no equipamento...'}
								autoCapitalize={'sentences'}
								autoCorrect={true}
								multiline={true}
								onChangeText={text => this.setState({ description: text })}
								value={this.state.description}></TextInput>
						</View>
					</View>

					{/* LOCALIZAÇÃO */}
					<View style={{ marginTop: 10 }}>
						<Text style={styles.formLabel}>Localização</Text>
						<View style={styles.formField}>
							<TextInput
								style={styles.formTextInput}
								placeholder={'Digite a localização do equipamento...'}
								autoCapitalize={'sentences'}
								autoCorrect={false}
								multiline={false}
								maxLength={50}
								onChangeText={text => this.setState({ location: text })}
								value={this.state.location}></TextInput>
						</View>
					</View>

					{/* FOTO */}
					<TouchableWithoutFeedback
						onPress={() => {
							this.takeImage();
						}}>
						<View style={{ marginTop: 10 }}>
							<Text style={styles.formLabel}>Foto</Text>
							<View style={[styles.formField, { padding: 15 }]}>
								<Text style={styles.formTextLabel}>{this.state.photoUri ? 'Trocar foto...' : 'Adicionar foto...'}</Text>
								<Image source={Images.icCamera}></Image>
							</View>
						</View>
					</TouchableWithoutFeedback>

					{this.state.photoUri && (
						<View style={{ flex: 1, height: 200, marginTop: 15 }}>
							<Image resizeMode={'cover'} source={{ uri: this.state.photoUri }} style={{ flex: 1 }} />
							<TouchableWithoutFeedback
								onPress={() => {
									this.removeImage();
								}}>
								<Text style={{ borderRadius: 6, padding: 8, textAlign: 'center', position: 'absolute', bottom: 10, right: 10, fontFamily: Fonts.nunitoSemiBold, color: Colors.white, fontSize: 12, backgroundColor: Colors.persianRed }}>REMOVER</Text>
							</TouchableWithoutFeedback>
						</View>
					)}

					<Button
						title={'Enviar'}
						style={{ marginTop: 30, marginBottom: 40 }}
						onPress={() => {
							this.sendInspection();
						}}></Button>
				</ScrollView>
			</SafeAreaView>
		);
	}

	async sendInspection() {
		try {
			if (await this.validate()) {
				if (this.state.photoPath) {
					const file = await RNFS.readFile(this.state.photoPath, 'base64');
					const imageBase64 = 'data:image/jpeg;base64,' + file;

					this.setState({ photo: imageBase64 });
				}

				await ControlInspection.insert(this.state);

				this.setState({ type: null, code: null, description: null, date: null, photoUri: null, photoPath: null, user: null, location: null });

				Alert.alert('Sucesso', 'Inspeção salva com sucesso');

				this.props.navigation.navigate('Home');
			}
		} catch (exception) {
			Alert.alert('Ops', 'Não foi possível salvar a inspeção. ' + exception.error);
		}
	}

	async validate() {
		const userName = await AsyncStorage.getItem('user_name');

		if (!userName) {
			Alert.alert('Atenção', 'Para cadastrar uma inspeção você primeiro configurar os dados do usuário.');
			return false;
		} else if (!this.state.type) {
			Alert.alert('Atenção', 'O campo tipo do equipamento deve ser preenchido.');
			return false;
		} else if (!this.state.code) {
			this.inputCode.focus();
			Alert.alert('Atenção', 'O campo código do equipamento deve ser preenchido.');
			return false;
		} else if (!this.state.description) {
			this.inputDescription.focus();
			Alert.alert('Atenção', 'O campo anomalias deve ser preenchido.');
			return false;
		}

		return true;
	}

	takeImage() {
		let options = {
			title: 'Foto da inspeção',
			chooseFromLibraryButtonTitle: 'Escolher da galeria',
			takePhotoButtonTitle: 'Tirar uma foto',
			cancelButtonTitle: 'Cancelar',
			mediaType: 'photo',
			noData: true,
			storageOptions: {
				skipBackup: true,
				path: 'images',
			},
		};

		ImagePicker.showImagePicker(options, response => {
			if (response.error) {
				Alert.alert('Ops', 'Não foi possível pegar uma foto. ' + response.error);
			} else if (response.didCancel) {
			} else {
				ImageResizer.createResizedImage(response.uri, 800, 800, 'JPEG', 80)
					.then(response => {
						this.setState({ photoPath: response.path, photoUri: response.uri });
					})
					.catch(err => {
						Alert.alert('Ops', 'Não foi possível pegar uma foto. ' + err);
					});
			}
		});
	}

	removeImage() {
		this.setState({ photoPath: null, photoUri: null });
	}
}

const styles = StyleSheet.create({
	formLabel: {
		fontFamily: Fonts.nunitoExtraBold,
		color: Colors.black,
		fontSize: 14,
	},

	formTextLabel: {
		flex: 1,
		fontFamily: Fonts.nunitoRegular,
		color: Colors.lightGray,
		fontSize: 14,
	},

	formField: {
		marginTop: 10,
		height: 48,
		backgroundColor: Colors.white,
		borderRadius: 4,
		alignItems: 'center',
		flexDirection: 'row',
	},

	formTextInput: {
		fontFamily: Fonts.nunitoRegular,
		color: Colors.black,
		fontSize: 14,
		flex: 1,
		paddingHorizontal: 15,
	},
});
