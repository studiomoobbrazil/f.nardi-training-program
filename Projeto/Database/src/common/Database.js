import SQLite from 'react-native-sqlite-storage';

export default class Database {
	static instance = null;

	static open() {
		if (this.instance == null) {

            console.log('Iniciando banco de dados...');

			this.instance = SQLite.openDatabase(
				{ name: 'db_inspector.sqlite', createFromLocation: 1 },
				() => {
                    console.log('Banco de dados iniciado com sucesso!');
				},
				error => {

                    console.error('Erro ao iniciar banco de dados: ' + error);
                }
			);
		}
		return this.instance;
	}

	static async select(sql, params) {
		try {
			const results = await this.execute(sql, params);
			return results;
		} catch (exception) {
			return exception;
		}
	}

	static async insert(sql, params) {
		try {
			const results = await this.execute(sql, params);
			return results.insertId;
		} catch (exception) {
			return exception;
		}
	}

	static async update(sql, params) {
		try {
			await this.execute(sql, params);
			return true;
		} catch (exception) {
			return exception;
		}
	}

	static async delete(sql, params) {
		try {
			await this.execute(sql, params);
			return true;
		} catch (exception) {
			return exception;
		}
	}

	static async bulk(sql, params) {
		try {
			await this.execute(sql, params);
			return true;
		} catch (exception) {
			return exception;
		}
	}

	static execute(sql, params) {
		return new Promise(function(resolve, reject) {
			Database.instance.transaction(sqliteTransaction => {
				try {
					sqliteTransaction.executeSql(
						sql,
						params,
						(transaction, results) => {
							resolve(results);
						},
						err => {
							reject(err);
						}
					);
				} catch (exception) {
					reject(exception);
				}
			});
		});
	}
}
