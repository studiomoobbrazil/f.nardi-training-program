import React, { Component } from 'react';
import { Text, View,SafeAreaView,Image,Alert } from 'react-native';

export default class ImageStatus extends Component {
    state = { status: '-' };

    render() {

        return (
            <SafeAreaView>
                <View style={{ flex: 1, flexDirection: 'column',alignItems: 'center'  }}>

                    <Text style={{width:100,height:100}}>{this.state.status}</Text>

                    <Image style={{ width: 320, height: 500 }} source={{ uri: "https://upload.wikimedia.org/wikipedia/commons/a/a9/Big_Banner_Example.png" }} onLoadStart={() => {

                        this.setState({ "status": "Start!" });                        

                    }}

                        onLoadEnd={() => {

                            this.setState({ "status": "Finish!" });                            

                        }} />
                </View>
            </SafeAreaView>
        );
    }
}