/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { SafeAreaView, Switch, View, Text, ScrollView, FlatList,Alert } from 'react-native';

class SelectPeriodComponent extends Component {
  constructor(props) {
    super(props);

    this.data = [{ id: 0, title: "Minas Gerais" }, { id: 1, title: "São Paulo" }, { id: 2, title: "Rio de Janeiro" }]
  }
  render() {

    return (
      <View style={{ flex: 1 }}>

        <FlatList data={this.data} renderItem={(item) => {

          <Text> {item.title}</Text>

        }} keyExtractor={(item) => {
          item.id
        }} />


Alert.alert(
  'Alert Title',
  'My Alert Msg',
  [
    {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
    {
      text: 'Cancel',
      onPress: () => console.log('Cancel Pressed'),
      style: 'cancel',
    },
    {text: 'OK', onPress: () => console.log('OK Pressed')},
  ],
  {cancelable: false},
);
      </View>
    );
  }
}

const App = () => {

  return (
    <SafeAreaView style={{ flex: 1, }}>
      <SelectPeriodComponent />
    </SafeAreaView>
  );
};

export default App;
