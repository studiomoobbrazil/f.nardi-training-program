import Database from '../common/Database';
import AsyncStorage from '@react-native-community/async-storage';

export default class ControlInspection {
	static async insert(data) {
		try {
			const SQL = 'INSERT INTO "inspection" ("type", "code", "description", "date", "photo", "user", "location","latitude","longitude") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';

			const date = Math.round(new Date().getTime() / 1000);

			const userName = await AsyncStorage.getItem('user_name');

			const params = [data.type, data.code, data.description, date, data.photo, userName, data.location,data.latitude,data.longitude];

			return await Database.insert(SQL, params);
		} catch (exception) {
			console.log(exception);
		}
	}

	static async list() {

		try {
		
			const SQL = 'SELECT * FROM "inspection" ORDER BY "date" DESC';

			const results = await Database.select(SQL);

			const data = results.rows.raw().map(row => {

				return {
					id: row.id,
					type: row.type,
					code: row.code,
					description: row.description,
					date: row.date,
					photo: row.photo,
					user: row.user,
					location: row.location,
					latitude: row.latitude,
					longitude: row.longitude,
				};
			});			
			return data;
		} catch (exception) {
			
			console.log(exception);
		}
	}

	static async delete(id) {
		try {
			const SQL = 'DELETE FROM "inspection" WHERE "id" = ?';

			const params = [id];

			return await Database.delete(SQL, params);
		} catch (exception) {
			console.log(exception);
		}
	}	
}
