import { format, fromUnixTime } from 'date-fns';
import React, { Component } from 'react';
import { Alert, Image, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Colors from '../../../common/Colors';
import Fonts from '../../../common/Fonts';
import Images from '../../../common/Images';
import ControlInspection from '../../../control/ControlInspection';
import Button from '../../components/Button';
import Header from '../../components/Header';

export default class DetailScreen extends Component {
	constructor(props) {
		super(props);

		this.data = this.props.navigation.getParam('data', null);
		this.formattedDate = format(fromUnixTime(this.data.date), 'dd/MM/yyyy HH:mm');
	}

	render() {
		return (
			<SafeAreaView style={{ flex: 1, backgroundColor: Colors.persianRed }}>
				<Header
					title={'Detalhe da inspeção'}
					leftButtonIcon={Images.icBack}
					leftButtonAction={() => {
						this.backAction();
					}}></Header>
				<ScrollView style={{ flex: 1, backgroundColor: Colors.extraLightGray, padding: 20 }}>
					{/* DATA */}
					<View style={{}}>
						<Text style={styles.label}>Data</Text>
						<Text style={styles.text}>{this.formattedDate}</Text>
					</View>

					{/* USUÁRIO */}
					<View style={{ marginTop: 15 }}>
						<Text style={styles.label}>Realizada por</Text>
						<Text style={styles.text}>{this.data.user}</Text>
					</View>

					{/* CÓDIGO */}
					<View style={{ marginTop: 15 }}>
						<Text style={styles.label}>Código do equipamento</Text>
						<Text style={styles.text}>{this.data.code}</Text>
					</View>

					{/* TIPO */}
					<View style={{ marginTop: 15 }}>
						<Text style={styles.label}>Tipo de equipamento</Text>
						<Text style={styles.text}>{this.data.type}</Text>
					</View>

					{/* LOCALIZAÇÃO */}
					{this.data.location && (
						<View style={{ marginTop: 15 }}>
							<Text style={styles.label}>Localização do equipamento</Text>
							<Text style={styles.text}>{this.data.location}</Text>
						</View>
					)}

					{/* ANOMALIAS */}
					<View style={{ marginTop: 15 }}>
						<Text style={styles.label}>Anomalias</Text>
						<Text style={styles.text}>{this.data.description}</Text>
					</View>

					{/* FOTO */}
					{this.data.photo && (
						<View style={{ flex: 1, height: 200, marginTop: 15 }}>
							<Text style={styles.label}>Foto</Text>
							<Image resizeMode={'cover'} source={{ uri: this.data.photo }} style={{ flex: 1, marginTop: 10 }} />
						</View>
					)}

					<Button
						title={'Excluir'}
						style={{ marginTop: 30 }}
						onPress={() => {
							this.deleteAction();
						}}></Button>
				</ScrollView>
			</SafeAreaView>
		);
	}

	backAction() {
		this.props.navigation.goBack(null);
	}

	deleteAction() {
		Alert.alert(
			'Inspector',
			'Tem certeza que deseja excluir esta inspeção?',
			[
				{
					text: 'Sim',
					onPress: () => {
						this.deleteInspection();
					},
				},
				{
					text: 'Não',
					style: 'cancel',
				},
			],
			{ cancelable: true }
		);
	}

	async deleteInspection() {
		try {
			await ControlInspection.delete(this.data.id);
			Alert.alert('Inspector', 'Inspeção excluida com sucesso');

			this.props.navigation.navigate('Home');
		} catch (exception) {
			Alert.alert('Ops', 'Não foi possível excluir a inspeção. ' + exception.error);
		}
	}
}

const styles = StyleSheet.create({
	label: {
		fontFamily: Fonts.nunitoExtraBold,
		color: Colors.black,
		fontSize: 16,
	},

	text: {
		fontFamily: Fonts.nunitoRegular,
		color: Colors.mediumGray,
		fontSize: 16,
		flex: 1,
	},
});
