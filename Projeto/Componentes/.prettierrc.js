module.exports = {
  "semi": true,
  "singleQuote": true,
  "trailingComma": "es5",
  "printWidth": 300,
  "tabWidth": 4,
  "useTabs": true,
  "bracketSpacing": true,
  "jsxBracketSameLine": true
};
